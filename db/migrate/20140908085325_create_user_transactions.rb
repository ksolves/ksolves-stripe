class CreateUserTransactions < ActiveRecord::Migration
  def change
    create_table :user_transactions do |t|
      t.integer :user_id
      t.integer :amount
      t.string :card_type
      t.string :last4
      t.string :transaction_id

      t.timestamps
    end
  end
end
