class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      	t.string    :login,               :null => false                # optional, you can use email instead, or both
      	t.string    :name,               :null => false                # optional, you can use email instead, or both

    	t.timestamps
    end
    
  end
end
