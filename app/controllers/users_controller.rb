class UsersController < ApplicationController
  def index
  end
  def add_balance
    token=params[:stripeToken]
    amount=params[:amount].to_f
    charge = Stripe::Charge.create(
      :amount => (amount*100.00).to_i, # amount in cents
      :currency => "usd",
      :card => token,
      :description => "test"
    )
    if charge.paid
      UserTransactions.create(user_id:current_user.id ,amount:amount ,card_type:charge.card.brand,last4:charge.card.last4 ,transaction_id:charge.id)
      notice="successful"
    else
      notice="Failed"
    end
    respond_to do |format|
      format.json{render json:{paid:charge.paid,notice:notice}}
    end
    #binding.pry
  end
  def show_account
    @transactions=UserTransactions.where("user_id=?",current_user.id) if current_user
  end
  def login
  	user=Users.where("login=?",params[:login]).last
  	if user && params[:login]==params[:password]
  		session[:user_id]=user.id
  	end
  	redirect_to(:back)
  end
  def logout
  	session[:user_id]=nil
    redirect_to '/users/index'
  end
end
